# 4 Game
In this game, given a target number n, the player must find the shortest mathematical expression that resolves to n using only the number 4. The player must also start their expression with the number 4.

## Operators
The mathematical operators allowed are as follows:
* \+4 (addition)
* \-4 (subtraction)
* \*4 (multiplication)
* \/4 (division)
* ^4 (power)

## Other options
In addition to these operators you can also:
* Concatenate a 4
* Concatenate .4 (add 0.4)
* Surround the current expression with parenthesis ()

## Aim of the project
This project aims to solve this problem algorithmically for any n value as input. It does this using the BFS (breadth first search) method. In addition, the program should also optionally accept as input a different starting digit (in the place of 4) to generalise the solution for all single digits.

## Usage
```
javac FourGame.java && java FourGame.java <target n> <starting n>
```