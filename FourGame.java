import java.util.ArrayList;

class State {
    String expression;

    public State(String expression) {
        this.expression = expression;
    }

    public ArrayList<State> generateMoves() {
        ArrayList<State> moves = new ArrayList<>();

        // Create a new state for each possible concatenation
        moves.add(new State(expression + "+" + FourGame.number));
        moves.add(new State(expression + "-" + FourGame.number));
        moves.add(new State(expression + "*" + FourGame.number));
        moves.add(new State(expression + "/" + FourGame.number));
        moves.add(new State(expression + "^" + FourGame.number));

        // Check that the current expression isn't surrounded with parenthesis
        if (!expression.endsWith(")")) {
            moves.add(new State(expression + FourGame.number));

            // Back track until we find something other than a 4
            for (int i = expression.length() - 1; i >= 0; i--) {
                if (!Character.toString(expression.charAt(i)).equals(Integer.toString(FourGame.number))) {
                    // As long as the current number we are looking at doesn't already have a
                    // decimal point, we can append .4
                    if (expression.charAt(i) != '.') {
                        moves.add(new State(expression + "." + FourGame.number));
                        i = -1; // Force loop to exit
                    } else {
                        // This number already has a decimal point
                        i = -1; // Force the loop to exit
                    }
                }
            }
        }
        moves.add(new State("(" + expression + ")"));

        return moves;
    }

    public static double eval(final String str) {
        return new Object() {
            int pos = -1, ch;

            void nextChar() {
                ch = (++pos < str.length()) ? str.charAt(pos) : -1;
            }

            boolean eat(int charToEat) {
                while (ch == ' ')
                    nextChar();
                if (ch == charToEat) {
                    nextChar();
                    return true;
                }
                return false;
            }

            double parse() {
                nextChar();
                double x = parseExpression();
                if (pos < str.length())
                    throw new RuntimeException("Unexpected: " + (char) ch);
                return x;
            }

            // Grammar:
            // expression = term | expression `+` term | expression `-` term
            // term = factor | term `*` factor | term `/` factor
            // factor = `+` factor | `-` factor | `(` expression `)` | number
            // | functionName `(` expression `)` | functionName factor
            // | factor `^` factor

            double parseExpression() {
                double x = parseTerm();
                for (;;) {
                    if (eat('+'))
                        x += parseTerm(); // addition
                    else if (eat('-'))
                        x -= parseTerm(); // subtraction
                    else
                        return x;
                }
            }

            double parseTerm() {
                double x = parseFactor();
                for (;;) {
                    if (eat('*'))
                        x *= parseFactor(); // multiplication
                    else if (eat('/'))
                        x /= parseFactor(); // division
                    else
                        return x;
                }
            }

            double parseFactor() {
                if (eat('+'))
                    return +parseFactor(); // unary plus
                if (eat('-'))
                    return -parseFactor(); // unary minus

                double x;
                int startPos = this.pos;
                if (eat('(')) { // parentheses
                    x = parseExpression();
                    if (!eat(')'))
                        throw new RuntimeException("Missing ')'");
                } else if ((ch >= '0' && ch <= '9') || ch == '.') { // numbers
                    while ((ch >= '0' && ch <= '9') || ch == '.')
                        nextChar();
                    x = Double.parseDouble(str.substring(startPos, this.pos));
                } else if (ch >= 'a' && ch <= 'z') { // functions
                    while (ch >= 'a' && ch <= 'z')
                        nextChar();
                    String func = str.substring(startPos, this.pos);
                    if (eat('(')) {
                        x = parseExpression();
                        if (!eat(')'))
                            throw new RuntimeException("Missing ')' after argument to " + func);
                    } else {
                        x = parseFactor();
                    }
                    if (func.equals("sqrt"))
                        x = Math.sqrt(x);
                    else if (func.equals("sin"))
                        x = Math.sin(Math.toRadians(x));
                    else if (func.equals("cos"))
                        x = Math.cos(Math.toRadians(x));
                    else if (func.equals("tan"))
                        x = Math.tan(Math.toRadians(x));
                    else
                        throw new RuntimeException("Unknown function: " + func);
                } else {
                    throw new RuntimeException("Unexpected: " + (char) ch);
                }

                if (eat('^'))
                    x = Math.pow(x, parseFactor()); // exponentiation

                return x;
            }
        }.parse();
    }

    public boolean isCorrect() {
        return State.eval(expression) == FourGame.target;
    }

    public String toString() {
        return expression;
    }
}

class FourGame {

    public static int number = 4;
    public static int target = 1;

    public static String breadthFirstSearch(String startingExpression) {

        ArrayList<State> frontier = new ArrayList<>();
        State current = new State(startingExpression);
        frontier.add(current);
        while (!frontier.isEmpty()) {

            // Pop current of the top of the list
            current = frontier.remove(0);

            // Check if current state is correct
            if (current.isCorrect()) {
                return current.toString();
            }

            // Otherwise expand this state
            ArrayList<State> moves = current.generateMoves();

            // Add these moves to the frontier
            frontier.addAll(moves);
        }
        return "";
    }

    public static void main(String[] args) {

        // Get target and starting numbers from command-line arguments
        if (args.length > 0) {
            try {
                FourGame.target = Integer.parseInt(args[0]);
            } catch (NumberFormatException e) {
                System.out.println("Please provide an integer for the target n.");
                System.exit(1);
            }
        }
        if (args.length > 1) {
            try {
                FourGame.number = Integer.parseInt(args[1]);
            } catch (NumberFormatException e) {
                System.out.println("Please provide an integer for the starting n.");
                System.exit(1);
            }
        }
        if (args.length == 0) {
            System.out.println("Usage: java FourGame.java <target n> <starting n>");
            System.exit(1);
        }

        System.out.println("Finding solution for the game starting with number " + FourGame.number + " targeting "
                + FourGame.target);

        System.out.println("Solution: " + breadthFirstSearch(Integer.toString(FourGame.number)));
    }
}